﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html"/>
  <xsl:template match="/">
  <html>
     <head>
       <title>ultima</title>
       <meta charset="UTF-8"/>
       <link rel="stylesheet" type="text/css" href="css/css.css"/>
     </head> 
     <body>
      <div class="header">
         <h1>GRABACIONES ANALOGICO / NXDN NEXEDGE</h1>       
       </div>
       <div class="izquierdo">
       <center>
         <table>
         <tr>
           <th>FICHERO</th>
           <th>REPRODUCCION</th>
           <th>EXTENSION</th>
           <th>FRECUENCIA_BASE</th>
           <th>NOMBRE</th>
           <th>PROTOCOLO</th>
         </tr>
         <xsl:for-each select="grabaciones/sistema">
         <xsl:sort select="./@formato" order="ascending" data-type="text"/>
         <xsl:if test="./@formato= 'analogico'">
            <tr>
            <td style="background-color: #541b57; color: #FFF; font-weight: bold:"><xsl:value-of select="fichero"/></td>
            <td style="background-color: #541b57; color: #FFF;">
             <audio>
               <xsl:attribute name="controls"/>
                 <source>
                   <xsl:attribute name="src">
                     <xsl:value-of select="fichero"/>
                       <xsl:text>.</xsl:text>
                       <xsl:value-of select="extension"/>
                       <xsl:attribute name="type">
                       <xsl:text>audio/wav</xsl:text>
                   </xsl:attribute>
                 </xsl:attribute>
               </source>
             </audio>          
           </td> 
           <td style="background-color: #541b57; color: #FFF;"><xsl:value-of select="extension"/></td>
           <td style="background-color: #541b57; color: #FFF;"><xsl:value-of select="freqbase"/></td>
           <td style="background-color: #541b57; color: #FFF;"><xsl:value-of select="nombre"/></td>
           <td style="background-color: #541b57; color: #FFF;"><xsl:value-of select="protocolo"/></td>
          </tr>     
         </xsl:if>
         
         <xsl:if test="./@formato != 'analogico'">
            <tr>
            <td><xsl:value-of select="fichero"/></td>
            <td>
             <audio>
               <xsl:attribute name="controls"/>
                 <source>
                   <xsl:attribute name="src">
                     <xsl:value-of select="fichero"/>
                       <xsl:text>.</xsl:text>
                       <xsl:value-of select="extension"/>
                       <xsl:attribute name="type">
                       <xsl:text>audio/wav</xsl:text>
                   </xsl:attribute>
                 </xsl:attribute>
               </source>
             </audio>          
           </td> 
           <td><xsl:value-of select="extension"/></td>
           <td><xsl:value-of select="freqbase"/></td>
           <td><xsl:value-of select="nombre"/></td>
           <td><xsl:value-of select="protocolo"/></td>
          </tr>     
         </xsl:if>
        
          </xsl:for-each>
       </table>
       </center>
       </div>
     </body>
   </html>
  </xsl:template>
</xsl:stylesheet>
