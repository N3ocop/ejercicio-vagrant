  apt-get update
  apt-get install net-tools --yes
  apt-get install apache2 --yes
  apt-get install vsftpd --yes
  cp -R /vagrant/web/* /var/www/html/
  cp /etc/vsftpd.conf /etc/vsftpd.conf_default
  systemctl start vsftpd
  systemctl enable vsftpd
  useradd -m userftp -p $6$Q9yH.pz0jWitug7Y$YIvDHiLYTKnUThgb0/dldZaVMaWmTI.N5A7kjAk/919ZU/Al1RusSsh2UktQtmPqbBPwwfKyu8N724ZOadRTa1 
  mkdir /home/userftp/ftp
  mkdir /home/userftp/ftp/files
  chown userftp:userftp /home/userftp/ftp/files
  chown nobody:nogroup /home/userftp/ftp
  chmod  a-w /home/userftp/ftp
  mkdir files
  cp /vagrant/vsftpd.conf /etc/
  systemctl restart vsftpd
  #Fin